# -*- coding: utf-8 -*-
from typing import List, Tuple, Callable, Dict, Any, Optional

import core
import umsgpack
from core.colors import Color
from core.commander import Commander
from core.data import print_menu, restore_menu, is_int
from pprint import pformat
from core.redis import Redis


def _print_learner() -> None:
    learner = core.data.LEARNER
    print('')
    Color.DARKCYAN.print(f' login:', oneline=True)
    Color.CYAN.print(f' {learner["login"]}', oneline=False)
    Color.DARKCYAN.print(f' nick:', oneline=True)
    Color.CYAN.print(f' {learner["nick"]}', oneline=False)
    Color.DARKCYAN.print(f' role:', oneline=True)
    Color.CYAN.print(f' {learner["role"]}', oneline=False)
    Color.DARKCYAN.print(f' uuid:', oneline=True)
    Color.CYAN.print(f' {learner["uuid"]}', oneline=False)
    print('')
    _update_main_menu()


def set_learner_by_nick() -> None:
    nick = Color.BLUE.prompt(f'Enter learners nick')
    if len(nick) < 3:
        Color.RED.print(' Invalid nick was provided')
    else:
        Color.GREEN.print(f' Searchnig for a nick...', oneline=True)

        by_nick = core.data.DB.get_user_by_nick(nick)

        if len(by_nick) < 1:
            Color.RED.print(f' NOT FOUND!')
        elif len(by_nick) > 1:
            Color.RED.print(f' TO MANY! Set by login.!')
        else:
            core.data.LEARNER = by_nick[0]
            _print_learner()

    restore_menu()


def set_learner_by_login() -> None:
    login = Color.BLUE.prompt(f'Enter learners login')
    if len(login) < 3:
        Color.RED.print(' Invalid login was provided')
    else:
        Color.GREEN.print(f' Searchnig for login...', oneline=True)

        by_login = core.data.DB.get_user_by_login(login)

        if len(by_login) < 1:
            Color.RED.print(f' NOT FOUND!')
        else:
            core.data.LEARNER = by_login[0]
            Color.GREEN.print(' FOUND!')
            _print_learner()

    restore_menu()


def set_learner_by_uuid() -> None:
    login = Color.BLUE.prompt(f'Enter learners uuid')
    if len(login) < 3:
        Color.RED.print(' Invalid uuid was provided')
    else:
        Color.GREEN.print(f' Searchnig for uuid...', oneline=True)

        by_uuid = core.data.DB.get_user_by_uuid(login)

        if len(by_uuid) < 1:
            Color.RED.print(f' NOT FOUND!')
        else:
            core.data.LEARNER = by_uuid[0]
            Color.GREEN.print(' FOUND!')
            _print_learner()

    restore_menu()


MAIN_LEARNER_MENU: List[Tuple[Callable, str]] = [
    (None, 'back to main menu'),
    (set_learner_by_nick, 'set by nick'),
    (set_learner_by_login, 'set by login'),
    (set_learner_by_uuid, 'set by uuid'),
]


def set_learner() -> None:
    print_menu(MAIN_LEARNER_MENU)


def handle_task(item: Dict[str, Any]):
    if item.get('type') != 'LIMBO':
        complete = Color.GREEN \
            .prompt('Would you like to complete this task? [y/N]')
        if complete.lower() == 'y':
            if item["type"] in ('INPUT', 'PRACTICE', 'PREPARETOCOLLABORATE',
                                'CONSOLIDATE', 'CHECKPOINT', 'REVIEW',
                                'DELIVER'):
                score = Color.BLUE \
                    .prompt('What score should i set? [default: 0.5]')
                duration = Color.BLUE \
                    .prompt('What duration should i set? [default: 20]')

                if not core.data.is_float(score):
                    score = 0.5

                if not core.data.is_int(duration):
                    duration = 20

                data = {
                    "action": "complete",
                    "learner": core.data.LEARNER['uuid'],
                    "task": item["name"],
                    "result": {
                        "scaled": float(score),
                        "duration": int(duration),
                        "success": True,
                        "completion": True
                    }
                }

                post = core.data.CMD.post(data)
                if 'outcome' in post:
                    Color.CYAN.print(f' Task completed!')
                else:
                    Color.RED.print(f' Broken pipe!')
                restore_menu()
            elif item["type"] in ['PREDELIVER']:
                data = {
                    "action": "complete",
                    "learner": core.data.LEARNER['uuid'],
                    "task": item["name"],
                    "result": {
                        "success": True,
                        "completion": True,
                        "deliverBooking": {
                            "teacherName": "MM",
                            "teacherId": "379",
                            "zamenhofId": "462428",
                            "zoomUrl": "https://zoom.us/j/616835245",
                            "date": "2018-08-29T12:00:00.000Z"
                        },
                        "duration": 30,
                        "scaled": 0
                    }
                }
                post = core.data.CMD.post(data)
                if 'outcome' in post:
                    Color.CYAN.print(f' Task completed!')
                else:
                    Color.RED.print(f' Broken pipe!')
                restore_menu()
            else:
                Color.RED.print(f' This type is not supported yet!')
                restore_menu()
        else:
            state = item['state'] if 'state' in item else 'unknown'

            if state != 'suspended':
                forfeit = Color.GREEN \
                    .prompt('Would you like to forfeit this task? [y/N]')

                if forfeit.lower() == 'y':
                    data = {
                        "action": "forfeit",
                        "learner": core.data.LEARNER['uuid'],
                        "task": item["name"],
                    }
                    post = core.data.CMD.post(data)
                    if 'outcome' in post:
                        Color.CYAN.print(f' Task forfeited!')
                        state = 'suspended'
                    else:
                        Color.RED.print(f' Broken pipe!')

            resumed: bool = False

            if state == 'suspended':
                resume = Color.GREEN \
                    .prompt('Would you like to resume '
                            'aka (go to previous) task? [y/N]')

                if resume.lower() == 'y':
                    data = {
                        "action": "resume",
                        "learner": core.data.LEARNER['uuid'],
                        "task": item["name"],
                    }
                    post = core.data.CMD.post(data)
                    if 'outcome' in post:
                        Color.CYAN.print(f' Task resumed!')
                        resumed = True
                    else:
                        Color.RED.print(f' Broken pipe!')

            if not resumed and item['type'] != 'SURVEY':
                forward = Color.GREEN \
                    .prompt('Would you like to fast forward  '
                            'aka (go to survey) from this task? [y/N]')

                if forward.lower() == 'y':
                    data = {
                        "action": "fast_forward",
                        "learner": core.data.LEARNER['uuid'],
                        "task": item["name"],
                    }
                    post = core.data.CMD.post(data)
                    if 'outcome' in post:
                        Color.CYAN.print(f' Task forwarded!')
                    else:
                        Color.RED.print(f' Broken pipe!')
    else:
        task_id = item.get('name')
        pair = Color.GREEN \
            .prompt('Would you like to pair with LC? [y/N]')

        if pair.lower() == 'y':
            course, module = core.data.CMD \
                .get_actual_module_4_task(item["name"])
            if course and module:
                teachers = core.data.DB.get_teachers()
                options: List[Tuple[Optional[str], str]] = [(None, 'Abort')]
                for t in teachers:
                    options.append((
                        t.get('uuid'),
                        f'{t.get("login")}, {t.get("uuid")}'
                    ))

                print('-----------------------------------')
                for index, item in enumerate(options):
                    print(f'  [{index}] - {item[1]}')
                print('-----------------------------------')

                option = Color.GREEN \
                    .prompt(f'Select option from range [0-{len(options)-1}]')

                if not is_int(option) or (int(option) > (len(options) - 1)):
                    Color.RED.print(' Unknown option. Aborting...')
                elif options[int(option)]:
                    if options[int(option)][0]:
                        partner = options[int(option)][0]
                        Color.DARKCYAN.print(f' partner:', oneline=True)
                        Color.CYAN.print(f' {partner}', oneline=False)
                        Color.DARKCYAN.print(f' course:', oneline=True)
                        Color.CYAN.print(f' {course}', oneline=False)
                        Color.DARKCYAN.print(f' module:', oneline=True)
                        Color.CYAN.print(f' {module}', oneline=False)
                        Color.DARKCYAN.print(f' task:', oneline=True)
                        Color.CYAN.print(f' {task_id}', oneline=False)

                        confirm = Color.GREEN.prompt('Is that correct? [y/N]')

                        if confirm.lower() == 'y':
                            data = {
                                "action": "resume",
                                "learner": core.data.LEARNER['uuid'],
                                "task": task_id,
                                "partner": partner,
                                "course": course,
                                "module": module
                            }

                            post = core.data.CMD.post(data)
                            if 'outcome' in post:
                                Color.CYAN.print(f' Pairing completed!')
                            else:
                                Color.RED.print(f' Broken pipe!')
            else:
                Color.RED.print(' Module or Course NOT FOUND!')


def get_learner_noticeboard() -> None:
    Color.BOLD.print(f' NOTICEBOARD of {core.data.LEARNER["login"]}')
    noticeboard = core.data.CMD.get_noticeboard(core.data.LEARNER['uuid'])
    if len(noticeboard):
        item = noticeboard[0]
        print('-----------------------------------')
        if 'name' in item:
            Color.DARKCYAN.print(f' name:', oneline=True)
            Color.CYAN.print(f' {item["name"]}', oneline=False)
        if 'type' in item:
            Color.DARKCYAN.print(f' type:', oneline=True)
            Color.CYAN.print(f' {item["type"]}', oneline=False)
        if 'assigned_date' in item:
            Color.DARKCYAN.print(f' assigned_date:', oneline=True)
            Color.CYAN.print(f' {item["assigned_date"]}', oneline=False)
        if 'launch_uri' in item:
            Color.DARKCYAN.print(f' launch_uri:', oneline=True)
            Color.CYAN.print(f' {item["launch_uri"]}', oneline=False)
        if 'done_date' in item:
            Color.DARKCYAN.print(f' done_date:', oneline=True)
            Color.CYAN.print(f' {item["done_date"]}', oneline=False)
        if 'misc_params' in item:
            params = item['misc_params']
            if 'name' in params:
                Color.DARKCYAN.print(f' name:', oneline=True)
                Color.CYAN.print(f' {params["name"]}', oneline=False)
            if 'course_name' in params:
                Color.DARKCYAN.print(f' course_name:', oneline=True)
                Color.CYAN.print(f' {params["course_name"]}', oneline=False)
                course = core.data.CMD.get_course_simple_name(
                    params["course_name"]
                )
                Color.DARKCYAN.print(f' course:', oneline=True)
                Color.CYAN.print(f' {course}', oneline=False)
            if 'module_name' in params:
                Color.DARKCYAN.print(f' module_name:', oneline=True)
                Color.CYAN.print(f' {params["module_name"]}', oneline=False)

        handle_task(item)

    else:
        Color.RED.print(' Noticeboard is EMPTY')
        Color.GREEN.print(f' Checking history items...')
        history = core.data.CMD.get_history(core.data.LEARNER['uuid'])
        if len(history):
            item = history[-1]
            print_history_item(item)
            handle_task(item)
        else:
            Color.RED.print(f' History is EMPTY!')
    restore_menu()


def print_history_item(item: Dict[str, Any]) -> None:
    if 'name' in item:
        Color.DARKCYAN.print(f' name:', oneline=True)
        Color.CYAN.print(f' {item["name"]}', oneline=False)
    if 'type' in item:
        Color.DARKCYAN.print(f' type:', oneline=True)
        Color.CYAN.print(f' {item["type"]}', oneline=False)
    if 'state' in item:
        Color.DARKCYAN.print(f' state:', oneline=True)
        Color.CYAN.print(f' {item["state"]}', oneline=False)
    if 'score' in item:
        Color.DARKCYAN.print(f' score:', oneline=True)
        Color.CYAN.print(f' {item["score"]}', oneline=False)
    if 'forfeited_count' in item:
        Color.DARKCYAN.print(f' forfeited_count:', oneline=True)
        Color.CYAN.print(f' {item["forfeited_count"]}', oneline=False)
    if 'assigned_date' in item:
        Color.DARKCYAN.print(f' assigned_date:', oneline=True)
        Color.CYAN.print(f' {item["assigned_date"]}', oneline=False)
    if 'misc_params' in item:
        params = item['misc_params']
        if 'name' in params:
            Color.DARKCYAN.print(f' name:', oneline=True)
            Color.CYAN.print(f' {params["name"]}', oneline=False)
        if 'course_name' in params:
            Color.DARKCYAN.print(f' course_name:', oneline=True)
            Color.CYAN.print(f' {params["course_name"]}', oneline=False)
            course = core.data.CMD.get_course_simple_name(
                params["course_name"]
            )
            Color.DARKCYAN.print(f' course:', oneline=True)
            Color.CYAN.print(f' {course}', oneline=False)
        if 'module_name' in params:
            Color.DARKCYAN.print(f' module_name:', oneline=True)
            Color.CYAN.print(f' {params["module_name"]}', oneline=False)


def get_learner_history() -> None:
    Color.BOLD.print(f' HISTORY of {core.data.LEARNER["login"]}')
    history = core.data.CMD.get_history(core.data.LEARNER['uuid'])
    if len(history):
        for item in history:
            print('-----------------------------------')
            print_history_item(item)
    restore_menu()


def retire() -> None:
    confirm = Color.GREEN.prompt('Are you sure that you want to retire learners state? [y/N]')

    if confirm.lower() == 'y':
        data = {
            "action": "retire",
            "learner": core.data.LEARNER['uuid']
        }
        post = core.data.CMD.post(data)
        if 'outcome' in post:
            Color.CYAN.print(f' Learner brain erased! ;)')
        else:
            Color.RED.print(f' Broken pipe!')

    restore_menu()


def reset_onboard() -> None:
    confirm = Color.GREEN \
        .prompt(f'Are you sure you want to set status of a learner to CREATED? [y/N]')

    if confirm.lower() == 'y':
        update = core.data.DB.set_learners_status(core.data.LEARNER['uuid'], 'CREATED')
        if update:
            Color.CYAN.print(f' Learner state set! ;)')
        else:
            Color.RED.print(f' Broken pipe!')
    restore_menu()


def onboard() -> None:
    options: List[Tuple[Optional[str], str]] = [(None, 'Abort')]
    for c in Commander.course_map.items():
        if c[1] != 'C0':
            options.append((
                c[0],
                f'{c[1]} - {c[0]}'
            ))

    print('-----------------------------------')
    for index, item in enumerate(options):
        print(f'  [{index}] - {item[1]}')
    print('-----------------------------------')

    Color.BOLD.print(' Select course')

    option = Color.GREEN \
        .prompt(f'Select option from range [0-{len(options)-1}]')

    if not is_int(option) or (int(option) > (len(options) - 1)):
        Color.RED.print(' Unknown option. Aborting...')
    elif options[int(option)]:
        if options[int(option)][0]:
            course_name = options[int(option)][0]
            course_hr_name = options[int(option)][1]
            course_uuid = None
            Color.BOLD.print(f' Fetching course configuration...')
            cv = core.data.CMD.get_curriculum()
            if cv:
                for key in cv.keys():
                    c = cv.get(key)
                    if c.get('name') == course_name:
                        course_uuid = c.get('id')
                        break

                if course_uuid:
                    modules = core.data.CMD.get_course_modules(course_uuid)
                    if modules:
                        modules = [('Abort', None)] + modules
                        print('-----------------------------------')
                        for index, item in enumerate(modules):
                            print(f'  [{index}] - {item[0]}')
                        print('-----------------------------------')
                        Color.BOLD.print(' Select module')
                        option = Color.GREEN \
                            .prompt(f'Select option from range [0-{len(modules)-1}]')

                        if not is_int(option) or (int(option) > (len(modules) - 1)):
                            Color.RED.print(' Unknown option. Aborting...')
                        elif modules[int(option)] and modules[int(option)][1]:
                            module = modules[int(option)]
                            Color.DARKCYAN.print(f' course:', oneline=True)
                            Color.CYAN.print(f' {course_hr_name} - {course_uuid}', oneline=False)
                            Color.DARKCYAN.print(f' module:', oneline=True)
                            Color.CYAN.print(f' {module[0]} - {module[1]}', oneline=False)
                            confirm = Color.GREEN.prompt('Is that correct? [y/N]')
                            if confirm.lower() == 'y':
                                data = {
                                    "action": "onboard",
                                    "learner": core.data.LEARNER['uuid'],
                                    "course": course_uuid,
                                    "module": module[1]
                                }
                                post = core.data.CMD.post(data)
                                if 'outcome' in post:
                                    Color.CYAN.print(f' Learner onboarded!')
                                else:
                                    Color.RED.print(f' Broken pipe!')

                    else:
                        Color.RED.print(f' Modules NOT FOUND!!')
                else:
                    Color.RED.print(f' Course configuration NOT FOUND!!')
            else:
                Color.RED.print(f' Broken pipe!')

    restore_menu()


def get_learner_redis() -> None:
    Color.BOLD.print(f' REDIS of {core.data.LEARNER["login"]}')
    redis_records = core.data.REDIS.get_user_redis_records(core.data.LEARNER['uuid'])
    if len(redis_records) > 0:
        for record in redis_records:
            Color.CYAN.print(f'{record}')
    else:
        Color.CYAN.print(f'None')
    print('-----------------------------------')
    restore_menu()


def get_learner_pickled_events() -> None:
    Color.YELLOW.print(f' Events for {core.data.LEARNER["login"]}')
    Color.YELLOW.print(f' ONBOARDING ')
    redis_list = core.data.REDIS.get_user_redis_events(
        'onboarding_rs', core.data.LEARNER['uuid'])

    if len(redis_list) == 0:
        Color.CYAN.print(f'None')
    else:
        Color.RED.print(f'This list should be empty. '
                        f'Potential error - investigate!!!')

    for idx, item in enumerate(redis_list):
        try:
            unpacked = umsgpack.unpackb(item)
            if not isinstance(unpacked, int):
                Color.DARKCYAN.print(f'Item {idx} unpacked:')
                human_readable = Redis.to_human_readable(unpacked)
                Color.CYAN.print(f'{pformat(human_readable)}')
        except ValueError:
            print(f"json check")
        except umsgpack.InsufficientDataException as e:
            print(f"InsufficientDataException  {e}")
        except TypeError as e:
            print(f"Wrong type while decoding  {e}")
    Color.YELLOW.print(f' STANDARD ')
    redis_list = core.data.REDIS.get_user_redis_events(
        'standard_rs', core.data.LEARNER['uuid'])

    if len(redis_list) == 0:
        Color.CYAN.print(f'None')
    else:
        Color.RED.print(f'This list should be empty. '
                        f'Potential error - investigate!!!')

    for idx, item in enumerate(redis_list):
        try:
            unpacked = umsgpack.unpackb(item)
            if not isinstance(unpacked, int):
                Color.DARKCYAN.print(f'Item {idx} unpacked:')
                human_readable = Redis.to_human_readable(unpacked)
                Color.CYAN.print(f'{pformat(human_readable)}')
        except ValueError:
            print(f"json check")
        except umsgpack.InsufficientDataException as e:
            print(f"InsufficientDataException  {e}")
        except TypeError as e:
            print(f"Wrong type while decoding  {e}")
    print('-----------------------------------')
    restore_menu()


def get_learner_pickled_facts() -> None:
    Color.YELLOW.print(f' Facts for {core.data.LEARNER["login"]}')
    Color.YELLOW.print(f' ONBOARDING ')
    redis_list = core.data.REDIS.get_user_redis_facts(
        'onboarding_rs', core.data.LEARNER['uuid'])

    if len(redis_list) == 0:
        Color.CYAN.print(f'None')

    facts_counter = 0
    for idx, item in enumerate(redis_list):
        try:
            unpacked = umsgpack.unpackb(item)
            if not isinstance(unpacked, int):
                Color.DARKCYAN.print(f'Item {idx} unpacked:')
                human_readable = Redis.to_human_readable(unpacked)
                Color.CYAN.print(f'{pformat(human_readable)}')
                facts_counter += 1
        except ValueError:
            print(f"json check")
        except umsgpack.InsufficientDataException as e:
            print(f"InsufficientDataException  {e}")
        except TypeError as e:
            print(f"Wrong type while decoding  {e}")

    if facts_counter > 1:
        Color.RED.print(f'This list should contain one element. '
                        f'Potential error - investigate!!!')

    Color.YELLOW.print(f' STANDARD ')
    redis_list = core.data.REDIS.get_user_redis_facts(
        'standard_rs', core.data.LEARNER['uuid'])

    if len(redis_list) == 0:
        Color.CYAN.print(f'None')

    facts_counter = 0
    for idx, item in enumerate(redis_list):
        try:
            unpacked = umsgpack.unpackb(item)
            if not isinstance(unpacked, int):
                Color.DARKCYAN.print(f'Item {idx} unpacked:')
                human_readable = Redis.to_human_readable(unpacked)
                Color.CYAN.print(f'{pformat(human_readable)}')
                facts_counter += 1
        except ValueError:
            print(f"json check")
        except umsgpack.InsufficientDataException as e:
            print(f"InsufficientDataException  {e}")
        except TypeError as e:
            print(f"Wrong type while decoding  {e}")

    if facts_counter > 1:
        Color.RED.print(f'This list should contain one element. '
                        f'Potential error - investigate!!!')

    print('-----------------------------------')
    restore_menu()


def get_learner_timers() -> None:
    Color.BOLD.print(f' TIMERS for {core.data.LEARNER["login"]}')
    timers_onboarding = core.data.REDIS.get_timers(
        'onboarding', core.data.LEARNER['uuid'])
    Color.DARKCYAN.print(f'Onboarding Timers:')
    if len(timers_onboarding) > 0:
        for timer in timers_onboarding:
            Color.CYAN.print(f'{timer}')
    else:
        Color.CYAN.print(f'None')

    timers_standard = core.data.REDIS.get_timers(
        'standard_rs', core.data.LEARNER['uuid'])
    Color.DARKCYAN.print(f'Standard Timers:')
    if len(timers_standard) > 0:
        for timer in timers_standard:
            Color.CYAN.print(f'{timer}')
    else:
        Color.CYAN.print(f'None')

    restore_menu()


MAIN_EXTENDED_MAIN_MENU: List[Tuple[Callable, str]] = [
    (None, 'exit'),
    (set_learner, 'change learner'),
    (get_learner_noticeboard, 'get noticeboard'),
    (get_learner_history, 'get history'),
    (retire, 'retire learner state in booker'),
    (onboard, 'onboard'),
    (reset_onboard, 'reset onboarding'),
    (get_learner_redis, 'get redis state'),
    (get_learner_pickled_events, 'get pickled events'),
    (get_learner_pickled_facts, 'get pickled facts'),
    (get_learner_timers, 'get timers')
]


def _update_main_menu():
    core.data.MEMORY_LIST = [MAIN_EXTENDED_MAIN_MENU]
    restore_menu()
