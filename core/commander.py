# -*- coding: utf-8 -*-
import json
from collections import defaultdict
from typing import Any, Dict, Optional, Tuple, List

from core.executor import Executor


class Commander:
    _booker = 'http://internal-prod-booker-alb-757451668.ap-northeast-1.elb.amazonaws.com:5000/'

    onboarding_course_id = '0ac1a222-fb95-41a2-912f-b3c458f89847'
    onboarding_module_id = '19e0ebae-758f-43c7-9a21-03e0b2cafabe'

    course_map = defaultdict(lambda: 'unknown')
    course_map['Course 0 - Onboarding'] = 'C0'
    course_map['Understand your own value'] = 'C1'
    course_map['Solve a project problem'] = 'C2'
    course_map['Improve team communication'] = 'C3'
    course_map['Participate in a negotiation'] = 'C4'
    course_map['Connect with a key client'] = 'C5'
    course_map['Respond to negative feedback'] = 'C6'
    course_map['Open a negotiation'] = 'TC1'
    course_map['Communicate with your team'] = 'TC2'
    course_map['Open a negotiation [Internal]'] = 'IC1'

    def __init__(self, exc: Executor) -> None:
        self._exc = exc
        self._curriculum: Optional[Dict[str, Any]] = None
        super().__init__()

    def _get(self, path: str):
        data = self._exc.execute(f'curl -X GET {self._booker}{path}')
        if data[1] == 0:
            return json.loads(data[0])
        return {}

    def post(self, data: Dict[str, any]):
        data = self._exc.execute(f'curl -X POST {self._booker}booker/events/manual -d \'{json.dumps(data)}\'')
        if data[1] == 0:
            return json.loads(data[0])
        return {}

    def set_completed(self, uuid: str):
        noticeboard = self._get(f'noticeboard/{uuid}')
        return noticeboard if noticeboard else []

    def get_noticeboard(self, uuid: str):
        noticeboard = self._get(f'noticeboard/{uuid}')
        return noticeboard if noticeboard else []

    def get_history(self, uuid: str):
        history = self._get(f'history/{uuid}')
        return history if history else []

    def get_curriculum(self) -> Dict[str, Any]:
        if self._curriculum:
            return self._curriculum

        curriculum = self._get('curriculum')
        onboarding = curriculum.pop('onboarding')
        if onboarding:
            onboarding['modules'][0]['id'] = self.onboarding_module_id
            curriculum[self.onboarding_course_id] = onboarding
        self._curriculum = curriculum
        return self._curriculum

    def get_module_number(self, course: str, module_uuid: str) -> int:
        index = -1
        cv = self.get_curriculum()
        crs = cv.get(course)
        if crs:
            modules = crs.get('modules')
            if modules:
                for idx, val in enumerate(modules):
                    if val.get('id') == module_uuid:
                        index = idx
                        break
        return index + 1

    def get_course_name(self, course: str):
        cv = self.get_curriculum()
        if course == 'onboarding':
            course = self.onboarding_course_id
        crs = cv.get(course)
        if crs:
            name = crs.get('name')
            return self.course_map[name]
        return 'unknown'

    def get_course_modules(self, course_uuid: str):
        cv = self.get_curriculum()
        if course_uuid == 'onboarding':
            course_uuid = self.onboarding_course_id
        crs = cv.get(course_uuid)
        if crs:
            modules = crs.get('modules')
            if modules:
                return [(x['name'], x['id']) for x in modules]
        return []

    def get_course_simple_name(self, course_name: str):
        if course_name in self.course_map:
            return self.course_map[course_name]
        return 'unknown'

    def get_course_full_name(self, course_simple_name: str):
        for key, value in self.course_map.items():
            if value == course_simple_name:
                return key
        return 'unknown'

    def get_module_name(self, course_uuid: str, module_uuid: str):
        if course_uuid == 'onboarding':
            course_uuid = self.onboarding_course_id
            module_uuid = self.onboarding_module_id
        modules = self.get_course_modules(course_uuid)
        for module in modules:
            if module[1] == module_uuid:
                return module[0]
        return 'unknown'

    def get_task_name(self, course_uuid, module_uuid, task_id):
        cv = self.get_curriculum()
        if course_uuid == 'onboarding':
            course_uuid = self.onboarding_course_id
            module_uuid = self.onboarding_module_id
        crs = cv.get(course_uuid)
        try:
            filtered_modules = [
                module
                for module in crs.get('modules')
                if module.get('id') == module_uuid]
            for activity in filtered_modules[0].get('activities'):
                if activity.get('id') == task_id:
                    return activity.get('name')
        except Exception as e:
            print(f'Error getting task name {e}')
        return 'unknown'

    def get_actual_module_4_task(self, task_id: str) -> Tuple:
        if task_id.startswith('C9E_PAIRING:'):
            task_id = task_id.replace('C9E_PAIRING:', '')
        cr = self.get_curriculum()
        c_uuid, c_uuid_tmp = None, None
        m_uuid, m_uuid_tmp = None, None
        if cr:
            for key in cr.keys():
                c_uuid_tmp = key
                modules = cr.get(key).get('modules')
                for mod in modules:
                    m_uuid_tmp = mod.get('id')
                    activities = mod.get('activities')
                    for act in activities:
                        if act.get('id') == task_id:
                            c_uuid = c_uuid_tmp
                            m_uuid = m_uuid_tmp
                            break
        return c_uuid, m_uuid
