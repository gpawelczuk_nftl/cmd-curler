
import base64
import core
from core.colors import Color


class Redis:
    def __init__(self, exc, host):
        # type: (Executor, str) -> None
        self._exc = exc
        self._cmd = f'redis-cli --raw -h {host} -p 6379 %s'
        super().__init__()

    def _query(self, query: str):
        q = self._cmd % query
        print(q)
        req = self._exc.execute(q)
        if req[1] == 0:
            try:
                return req[0]
            except Exception:
                pass
        return None

    def _query_decoded(self, query: str):
        q = self._cmd % query
        print(q)
        req = self._exc.execute(q)
        if req[1] == 0:
            for data in req:
                try:
                    return base64.b64decode(data)
                except TypeError as e:
                    Color.RED.print(f'Error while decoding base64 {e}')
                    pass

        return None

    def get_user_redis_records(self, uuid):
        query = self._query(f'scan 0 MATCH *{uuid}* count 1000')
        if not query:
            return []
        return query.split('\n')

    def get_user_redis_events(self, ruleset, uuid):
        query = self._query_decoded(f'hgetall {ruleset}!e!{uuid} | base64')
        if not query:
            return []
        return Redis.split_query(query)

    def get_user_redis_facts(self, ruleset, uuid):
        query = self._query_decoded(f'hgetall {ruleset}!f!{uuid} | base64')
        if not query:
            return []
        return Redis.split_query(query)

    def get_timers(self, ruleset_name, uuid) -> []:
        response = self._query(f'zrange {ruleset_name}!t 0 -1')
        if not response:
            return []
        return [
            item for item in response.split('\n') if uuid in item
        ] if isinstance(response, str) else []

    @staticmethod
    def split_query(query_bytes):
        if not query_bytes:
            return []
        data = query_bytes.split(b'\n')
        return [i for i in data if i != b'']

    @staticmethod
    def to_human_readable(unpacked):
        try:
            course = unpacked['course']
            module = unpacked['module']
            task = unpacked['task']
            course_name = core.data.CMD.get_course_name(course)
            course_full_name = core.data.CMD.get_course_full_name(course_name)
            module_name = core.data.CMD.get_module_name(course, module)
            task_name = core.data.CMD.get_task_name(course, module, task)
            unpacked['course'] = f'{course_name}: {course_full_name}'
            unpacked['module'] = f'{module_name}'
            unpacked['task'] = f'{task_name}'
        except KeyError as e:
            print(f"Error while reading {e}")
            return unpacked
        return unpacked
