# -*- coding: utf-8 -*-
from typing import List, Tuple, Callable, Any, Dict, Optional

import core
from core.colors import Color
from core.commander import Commander
from core.db import Sql
from core.redis import Redis
from core.executor import Executor

MEMORY_LIST: List[List[Tuple[Callable, str]]] = []
MEMORY_LIST_INDEX: int = -1
LEARNER: Optional[Dict[str, Any]] = None

HOST = 'sg1-continuum.prod.aws.nftl.cloud'
USER = 'gpawelczuk'
KEY = '/Users/gpawelczuk/.ssh/id_rsa_nftl.pub'
DB_HOST = 'prod-users-db.cqeo53qv52u5.ap-northeast-1.rds.amazonaws.com'
DB_PASS = 'setme'
DB_USER = 'babylon'
DATABASE = 'postgres'
REDIS_HOST = 'set me too'

shell_exc = None
db_exc = None
redis_exc = None

CMD = None
DB = None
REDIS = None


def restore_menu() -> None:
    if len(core.data.MEMORY_LIST) > 0:
        menu = core.data.MEMORY_LIST.pop()
        print_menu(menu)
        if len(core.data.MEMORY_LIST) == 0:
            core.data.MEMORY_LIST.append(menu)


def print_menu(menu: List[Tuple[Callable, str]]) -> None:
    print('-----------------------------------')
    for index, item in enumerate(menu):
        print(f'  [{index}] - {item[1]}')
    print('-----------------------------------')
    option = Color.GREEN.prompt(f'Select option from range [0-{len(menu)-1}]')

    if not is_int(option) or (int(option) > (len(menu) - 1)):
        Color.RED.print(' Invalid option selected, let\'s try again...')
        print_menu(menu)
    elif menu[int(option)]:
        if int(option) != 0:
            core.data.MEMORY_LIST.append(menu)
        if menu[int(option)][0]:
            menu[int(option)][0]()
        else:
            restore_menu()


def is_int(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


def is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False
