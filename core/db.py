# -*- coding: utf-8 -*-
import json
from typing import Any, Dict, Optional, List

from core.executor import Executor


class Sql:
    def __init__(self, exc, host, db, usr, pwd):
        # type: (Executor, str, str, str, str) -> None
        self._exc = exc
        self._cmd = f'export PGPASSWORD={pwd};psql -U {usr} -h {host} -d {db} -P pager -t -A -c "%s"'
        super().__init__()

    def _query(self, query: str) -> Optional[List[Dict[str, Any]]]:
        q = self._cmd % f'with t as ({query}) select json_agg(t) from t;'
        # print (q)
        req = self._exc.execute(q)
        if req[1] == 0:
            try:
                return json.loads(req[0])
            except Exception:
                pass
        return None

    def _simple_query(self, query: str) -> Any:
        q = self._cmd % f'{query};'
        req = self._exc.execute(q)
        if req[1] == 0:
            try:
                return req[0]
            except Exception:
                pass
        return None

    def get_user_by_uuid(self, uuid):
        query = self._query(f'SELECT * from users where uuid = \'{uuid}\'')
        if not query:
            return []
        return query

    def get_user_by_nick(self, nick):
        query = self._query(f'SELECT * from users where nick = \'{nick}\'')
        if not query:
            return []
        return query

    def get_user_by_login(self, login):
        query = self._query(f'SELECT * from users where login = \'{login}\'')
        if not query:
            return []
        return query

    def get_teachers(self):
        query = self._query(f'SELECT * from users where role = \'TEACHER\'')
        if not query:
            return []
        return query

    def set_learners_status(self, uuid, status) -> bool:
        query = self._simple_query(f'UPDATE users SET status=\'{status}\' where uuid = \'{uuid}\'')
        if not query:
            return False
        return True
