# -*- coding: utf-8 -*-
from enum import Enum


class Color(Enum):
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

    def print(self, text: str, oneline: bool=False) -> None:
        if not oneline:
            print(f'{self.value}{text}\033[0m')
        else:
            print(f'{self.value}{text}\033[0m', end="")

    def prompt(self, text: str) -> str:
        return input(f'{self.value} {text}: \033[0m')
