# -*- coding: utf-8 -*-
from typing import Tuple, cast
import paramiko


class Executor:
    def __init__(self, user, host, pub_key):
        # type: (str,str,str) -> None
        self._connection = paramiko.SSHClient()
        self._connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self._user = user
        self._host = host
        self._pub = pub_key
        super().__init__()

    def connect(self) -> None:
        self._connection.connect(
            self._host,
            username=self._user,
            key_filename=self._pub
        )

    def disconnect(self) -> None:
        self._connection.close()

    def execute(self, cmd: str) -> Tuple[str, int]:
        stdin, stdout, stderr = self._connection.exec_command(cmd)
        status = cast(int, stdout.channel.recv_exit_status())
        lines = stdout.readlines()
        return ''.join(lines), status
