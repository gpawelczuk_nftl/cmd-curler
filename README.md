# Before we start...

In core/data.py set:
```
DB_PASS - password for database connection
USER - username for ssh connection
KEY - path to public key that will be used for ssh auth
```