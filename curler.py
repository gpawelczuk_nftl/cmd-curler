# -*- coding: utf-8 -*-
from typing import List, Tuple, Callable

import core
from core.commander import Commander
from core.data import print_menu
from core.db import Sql
from core.executor import Executor
from core.redis import Redis
from opt import learner


def connect() -> None:
    core.data.shell_exc = Executor(core.data.USER, core.data.HOST, core.data.KEY)
    core.data.db_exc = Executor(core.data.USER, core.data.HOST, core.data.KEY)
    core.data.redis_exc = Executor(core.data.USER, core.data.HOST, core.data.KEY)
    core.data.shell_exc.connect()
    core.data.db_exc.connect()
    core.data.redis_exc.connect()
    core.data.CMD = Commander(core.data.shell_exc)
    core.data.DB = Sql(core.data.db_exc, core.data.DB_HOST, core.data.DATABASE, core.data.DB_USER, core.data.DB_PASS)
    core.data.REDIS = Redis(core.data.redis_exc, core.data.REDIS_HOST)


def configure_test() -> None:
    Commander._booker = 'http://internal-test-booker-alb-592056245.eu-west-1.elb.amazonaws.com:5000/'
    core.data.HOST = 'sg1-continuum.test.aws.nftl.cloud'
    core.data.DB_HOST = 'test-users-db.crkq30jv4fow.eu-west-1.rds.amazonaws.com'
    core.data.REDIS_HOST = 'test-cache.pspbwy.0001.euw1.cache.amazonaws.com'
    connect()
    print_menu(MAIN_MENU)


def configure_prod() -> None:
    Commander._booker = 'http://internal-prod-booker-alb-757451668.ap-northeast-1.elb.amazonaws.com:5000/'
    core.data.HOST = 'sg1-continuum.prod.aws.nftl.cloud'
    core.data.DB_HOST = 'prod-users-db.cqeo53qv52u5.ap-northeast-1.rds.amazonaws.com'
    core.data.REDIS_HOST = 'nftl-prod-repl-group.89fmhn.ng.0001.apne1.cache.amazonaws.com'
    connect()
    print_menu(MAIN_MENU)


START_MENU: List[Tuple[Callable, str]] = [
    (None, 'exit'),
    (configure_prod, 'Use \033[91mProd\033[0m Env'),
    (configure_test, 'Use \033[94mTest\033[0m Env')
]

MAIN_MENU: List[Tuple[Callable, str]] = [
    (None, 'exit'),
    (learner.set_learner, 'set learner')
]


def main() -> None:
    print_menu(START_MENU)


if __name__ == '__main__':
    main()
